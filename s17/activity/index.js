/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function info(){
		alert("enter you details");

		let fullname = prompt("Enter your Full Name");
		let age = prompt("Enter your age");
		let city = prompt("Enter the city you live in");

		console.log("Hello, " + fullname);
		console.log("you are "+age+" years old");
        console.log("you live in " + city);

	};

	info();



/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favartists(){
		

		console.log("1.A.R. Rahman");
		console.log("2.Justin Beiber");
		console.log("3.DSP ");
		console.log("4.MJ");
		console.log("5. anirudh");

		

	};

	favartists();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function favmovies(){
		

		console.log("1.Endgame");
		console.log("Rotten Tomatoes Rating: 94");
		console.log("2. Ford vs Ferrari ");
		console.log("Rotten Tomatoes Rating: 92");
		console.log("3. Inception");
		console.log("Rotten Tomatoes Rating: 91");
		console.log("4. Aakasham Nee Haddhura");
		console.log("Rotten Tomatoes Rating: 95");
		console.log("5. Rangasthalam");
		console.log("Rotten Tomatoes Rating: 90");

		

	};

	favmovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


    let printfriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printfriends();
// console.log(friend1);
// console.log(friend2);
