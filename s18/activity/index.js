/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function*/
  
function add_numbers(num_1,num_2){

	console.log("displayed sum of "+num_1+"and"+num_2+" is: ");
	console.log(num_1+num_2);
}

add_numbers(1, 2);


  
function diff_numbers(num_1,num_2){

	console.log("displayed difference of "+num_1+"and"+num_2+" is: ");
	console.log(num_1-num_2);
}

diff_numbers(5, 3);



/*
	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.*/
   

function mul_numbers(num_1,num_2){

	console.log("displayed product of "+num_1+"and"+num_2+" is: ");
	return num_1*num_2;
}


let product = mul_numbers(5, 10);
console.log(product);

function div_numbers(num_1,num_2){

	console.log("displayed quotient of "+num_1+"and"+num_2+" is: ");
	return num_1/num_2;
}

let quotient = div_numbers(12, 3);
console.log(quotient);





/*

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.*/

function areaOfCircle(radius) {
  let pi = Math.PI;
  let area = pi * Math.pow(radius, 2);
  return area;
}

let circleArea = areaOfCircle(15);
console.log("the area of circle with radius 15 is: ")
console.log(circleArea);



/*	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.*/
	
function average(n_1,n_2,n_3,n_4){
 let averageValue = (n_1+n_2+n_3+n_4)/4;

  return averageValue;
}


let averageValue = average(5,10,15,30);
console.log("the average value of 5,10,15 and 30 is: ")
console.log(averageValue);




	/*5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/


function checkPassingScore(score, totalScore) {
  let percentage = (score / totalScore) * 100;
  let isPassed = percentage > 75;
  return isPassed;
}

let isPassingScore = checkPassingScore(38, 50);
console.log("is 38 out of 50 passing score?")
console.log(isPassingScore);
