let trainer = {
    name: "Venkat",
  age: 19,
  pokemon: ["pikachu",'charizard','squirtle'],
  friends: {
    close: ["Ash", "May"]
  },
  
  talk: function() {
    console.log("Pikachu! I choose you!");
  }
};

function Pokemon(name, level) {
    this.name = name;
  this.level = level;
  this.health = 5 * level; 
  this.attack = 2 * level; 
  this.tackle = function(targetPokemon) {
    targetPokemon.health -= this.attack;
    
    
    if (targetPokemon.health <= 0) {
      this.faint(targetPokemon);
    }
  };
  
  this.faint = function(targetPokemon) {
    console.log(targetPokemon.name + " has fainted.");
  };
}

let pikachu = new Pokemon("Pikachu", 10);
let charizard = new Pokemon("Charizard", 50);
let squirtle = new Pokemon("Squirtle", 20);

console.log(trainer.name); 
console.log(trainer["age"]); 
console.log(trainer.pokemon); 


trainer.talk(); 


pikachu.tackle(charizard);


console.log(charizard.health); 
console.log(charizard.attack); 