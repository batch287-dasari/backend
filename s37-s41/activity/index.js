const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors")

const app = express();

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.h9zcbv5.mongodb.net/s37-activity",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once("open", () => console.log('Now connected in the cloud.'))

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});