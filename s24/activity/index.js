console.log('hello_world');
const num = 5;
const getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}`);


const address = ["Washington Street", "NYC", "USA"];
const [street, city, country] = address;
console.log(`The full address is ${street}, ${city}, ${country}`);


const animal = {
  name: "Lion",
  type: "Mammal",
  habitat: "Grasslands",
};


const { name, type, habitat } = animal;
console.log(`The animal details are: Name: ${name}, Type: ${type}, Habitat: ${habitat}`);


const numbers = [1, 2, 3, 4, 5];
numbers.forEach((num) => console.log(num));


const reduceNumber = numbers.reduce((sum, num) => sum + num, 0);
console.log(reduceNumber);


class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

const dog = new Dog("puppy", 3, "chihuahua");
console.log(dog);
