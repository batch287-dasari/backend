console.log("Hello World");

/*
	1. Create variables to store to the following user details:*/

	let first_name = "Dasari";
	console.log("First Name : "+first_name);
	let last_name = "Venkata Sai";
	console.log("Last Nmae : "+last_name);
let	age = 20;
console.log("age : "+age);
	let hobbies = ["Fooz ball", "Badminton", "Watching Movies", "Reading Books"];
	console.log("Hobbies are ");
	console.log(hobbies);
let	work_address = {

		houseNumber: "26-3-36",
		street: "main street",
		city: "pkl",
		state: "AP",
		
	}
	console.log("work adress is ");
	console.log(work_address);

		/*-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony","Bruce",'Thor','Natasha',"Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let FullName = "Bucky Barnes";
	console.log("My bestfriend is: " + FullName);

	const lastLocation = "Arctic Ocean";
	
	console.log("I was found frozen in: " + lastLocation);

