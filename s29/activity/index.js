db.users.find({ $or : [ { firstName: { $regex: 's', $options: 'i'}}, { lastName: { $regex: 'd', $options: 'i'}}]},
  {
    _id: 0,
    firstName: 1,
    lastName: 1
  }).pretty();


db.users.find({ $and : [ { age: {$gte: 70} }, { department: "HR" }]}).pretty();


db.users.find({ $and : [ { age: {$lte: 30} }, { firstName: { $regex: 'e', $options: 'i'}}]}).pretty();
